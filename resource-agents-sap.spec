#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.
#

# Below is the script used to generate a new source file
# from the resource-agent upstream git repo.
#
# Use a specified version as HEAD if needed, e.g. "v4.14.0"
#
# # Source0:
# git checkout <version tag>
# TAG=$(git log --pretty="format:%h" -n 1)
# distdir="ClusterLabs-resource-agents-${TAG}"
# TARFILE="${distdir}.tar.gz"
# rm -rf $TARFILE $distdir
# git archive --prefix=$distdir/ HEAD | gzip > $TARFILE
#
# # Source1:
# git checkout <version tag>
# TAG=$(git log --pretty="format:%h" -n 1)
# distdir="SAPStartSrv-resourceAgent-${TAG}"
# TARFILE="${distdir}.tar.gz"
# rm -rf $TARFILE $distdir
# git archive --prefix=$distdir/ HEAD | gzip > $TARFILE
#

%global upstream_prefix ClusterLabs-resource-agents
%global upstream_version a6ccb93a

%global sapstart_prefix SAPStartSrv-resourceAgent
%global sapstart_version 1758e33

# Whether this platform defaults to using systemd as an init system
# (needs to be evaluated prior to BuildRequires being enumerated and
# installed as it's intended to conditionally select some of these, and
# for that there are only few indicators with varying reliability:
# - presence of systemd-defined macros (when building in a full-fledged
#   environment, which is not the case with ordinary mock-based builds)
# - systemd-aware rpm as manifested with the presence of particular
#   macro (rpm itself will trivially always be present when building)
# - existence of /usr/lib/os-release file, which is something heavily
#   propagated by systemd project
# - when not good enough, there's always a possibility to check
#   particular distro-specific macros (incl. version comparison)
%define systemd_native (%{?_unitdir:1}%{!?_unitdir:0}%{nil \
  } || %{?__transaction_systemd_inhibit:1}%{!?__transaction_systemd_inhibit:0}%{nil \
  } || %(test -f /usr/lib/os-release; test $? -ne 0; echo $?))

# determine the ras-set to process based on configure invocation
%bcond_with rgmanager
%bcond_without linuxha

Name:     resource-agents-sap
Summary:  SAP cluster resource agents
Version:  4.15.1
Release:  1%{?rcver:%{rcver}}%{?numcomm:.%{numcomm}}%{?alphatag:.%{alphatag}}%{?dirty:.%{dirty}}%{?dist}
License:  GPLv2+
URL:      https://github.com/ClusterLabs/resource-agents
Source0:  %{upstream_prefix}-%{upstream_version}.tar.gz
Source1:  %{sapstart_prefix}-%{sapstart_version}.tar.gz

BuildArch:  noarch

# Build dependencies
BuildRequires: make
BuildRequires: automake autoconf pkgconfig gcc
BuildRequires: libxslt glib2-devel libqb-devel
BuildRequires: systemd
BuildRequires: which

%if 0%{?fedora} || 0%{?centos} > 7 || 0%{?rhel} > 7 || 0%{?suse_version}
BuildRequires: python3-devel
%else
BuildRequires: python-devel
%endif

%if 0%{?fedora} || 0%{?centos} || 0%{?rhel}
BuildRequires: docbook-style-xsl docbook-dtds
%if 0%{?rhel} == 0
BuildRequires: libnet-devel
%endif
%endif

%if 0%{?suse_version}
BuildRequires:  libnet-devel
%if 0%{?suse_version} > 1500
BuildRequires:  cluster-glue-devel
%else
BuildRequires:  libglue-devel
%endif
BuildRequires:  libxslt docbook_4 docbook-xsl-stylesheets
%endif

BuildRequires:  python3-psutil

Requires: resource-agents >= 4.8.0
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd

Requires: /bin/bash /usr/bin/grep /bin/sed /bin/gawk
Requires: python3 python3-psutil

%description
The SAP resource agents interface with Pacemaker to allow
SAP instances to be managed in a cluster environment.

%prep
%setup -q -n %{upstream_prefix}-%{upstream_version}
%setup -q -T -D -a 1 -n %{upstream_prefix}-%{upstream_version}

# Move the agent to the others for the ClusterLabs included processing.
mv %{sapstart_prefix}-%{sapstart_version}/ra/SAPStartSrv.in heartbeat

# Find the existing SAPInstance entry in the list and add 2 new after in corresponding formatting.
# heartbeat/Makefile.am indents by 3 tabs in the target list
# Skip the .in suffix, the agent is renamed in the process.
sed -i -e 's/\(\t\tSAPInstance\t\t\\\)/\1\n\t\t\tSAPStartSrv\t\t\\/' heartbeat/Makefile.am

# Find the existing SAPInstance entry in the list and add 2 new after in corresponding formatting.
# doc/man/Makefile.am indents by 26 spaces in the target list
sed -i -e 's/\( \{26\}ocf_heartbeat_SAPInstance.7 \\\)/\1\n'\
'                          ocf_heartbeat_SAPStartSrv.7 \\/' doc/man/Makefile.am

# Add RA to configure.ac for processing during compilation.
# - renames python files after shebang fixup
# - enables man page creation
sed -i -e 's/\(AC_CONFIG_FILES..heartbeat.SysInfo.*\)/AC_CONFIG_FILES([heartbeat\/SAPStartSrv], [chmod +x heartbeat\/SAPStartSrv])\n\1/' configure.ac

%build
if [ ! -f configure ]; then
  ./autogen.sh
fi

%if 0%{?fedora} >= 11 || 0%{?centos} > 5 || 0%{?rhel} > 5
CFLAGS="$(echo '%{optflags}')"
%global conf_opt_fatal "--enable-fatal-warnings=no"
%else
CFLAGS="${CFLAGS} ${RPM_OPT_FLAGS}"
%global conf_opt_fatal "--enable-fatal-warnings=yes"
%endif

%if %{with rgmanager}
%global rasset rgmanager
%endif
%if %{with linuxha}
%global rasset linux-ha
%endif
%if %{with rgmanager} && %{with linuxha}
%global rasset all
%endif

export CFLAGS

%configure \
%if 0%{?fedora} || 0%{?centos} > 7 || 0%{?rhel} > 7 || 0%{?suse_version}
  PYTHON="%{__python3}" \
%endif
  %{conf_opt_fatal} \
%if %{defined _unitdir}
    --with-systemdsystemunitdir=%{_unitdir} \
%endif
%if %{defined _tmpfilesdir}
    --with-systemdtmpfilesdir=%{_tmpfilesdir} \
    --with-rsctmpdir=/run/resource-agents \
%endif
  --with-pkg-name=resource-agents \
  --with-ras-set=%{rasset}

make %{_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# Remove other agents
find %{buildroot}/usr/lib/ocf ! -type d ! -iname "SAP*" -exec rm {} \;
find %{buildroot}%{_mandir} -type f ! -iname "*SAP*" -exec rm {} \;
find %{buildroot}%{_unitdir} -type f ! -iname "sap*" -exec rm {} \;

# Service units and script called by services.
install -D -m 0644 %{sapstart_prefix}-%{sapstart_version}/sbin/sapservices-move.in \
  %{buildroot}%{_bindir}/sapservices-move
install -D -m 0644 %{sapstart_prefix}-%{sapstart_version}/service/* \
  %{buildroot}%{_unitdir}

# Change hard-coded /usr/sbin to /usr/bin, where the sapservices-move script lives.
sed -i -e 's|%{_sbindir}|%{_bindir}|g' %{buildroot}%{_unitdir}/sap*

# Fix python shebang.
sed -i -e 's|@PYTHON@|%{__python3}|g' %{buildroot}%{_bindir}/sapservices-move

# License
mkdir -p %{buildroot}%{_defaultlicensedir}/%{name}
cp %{sapstart_prefix}-%{sapstart_version}/LICENSE %{buildroot}%{_defaultlicensedir}/%{name}

## tree fixup
# remove docs (there is only one and they should come from doc sections in files)
rm -rf %{buildroot}/usr/share/doc/resource-agents

%post
%systemd_post sapping.service
%systemd_post sappong.service

%preun
%systemd_preun sapping.service
%systemd_preun sappong.service

%postun
%systemd_postun sapping.service
%systemd_postun sappong.service

%files
%defattr(-,root,root)
%license LICENSE
%{_usr}/lib/ocf/resource.d/heartbeat/SAP*
%{_usr}/lib/ocf/lib/heartbeat/sap*
%{_mandir}/man7/*SAP*
%defattr(755,root,root,-)
%{_bindir}/sap*
%defattr(644,root,root,-)
%{_unitdir}/sap*

%exclude /etc
%exclude /usr/include
%exclude %{_usr}/lib/debug
%exclude %{_usr}/lib/tmpfiles.d
%exclude /usr/libexec/heartbeat
%exclude /usr/sbin/ldirectord
%exclude /usr/sbin/ocf*
%exclude /usr/share/resource-agents
%exclude /usr/share/pkgconfig/resource-agents.pc
%exclude /usr/src

%changelog
* Fri Jan 10 2025 Janine Fuchs <jfuchs@redhat.com> - 4.15.1-1
- Add resource agent SAPStartSrv.
  Resolves: RHEL-2294

* Wed Dec 07 2022 Janine Fuchs <jfuchs@redhat.com> - 4.10.0-3
- SAPInstance: be more resilient against broken kill.sap files

  Resolves: rhbz#2151293

* Mon Nov 22 2021 Oyvind Albrigtsen <oalbrigt@redhat.com> - 4.10.0-2
- Rebase to resource-agents 4.10.0 upstream release.

  Resolves: rhbz#2024660

* Tue Aug 10 2021 Mohan Boddu <mboddu@redhat.com> - 4.8.0-2.1
- Rebuilt for IMA sigs, glibc 2.34, aarch64 flags
  Related: rhbz#1991688

* Tue Jun 22 2021 Oyvind Albrigtsen <oalbrigt@redhat.com> - 4.8.0-2
- Add CI gating tests

  Resolves: rhbz#1960245

* Mon Jun  7 2021 Oyvind Albrigtsen <oalbrigt@redhat.com> - 4.8.0-1
- Rebase to resource-agents 4.8.0 upstream release.

* Fri Apr 17 2020 Oyvind Albrigtsen <oalbrigt@redhat.com> - 4.1.1-30
- use safe temp file location

  Resolves: rhbz#1817439

* Thu Jan 23 2020 Oyvind Albrigtsen <oalbrigt@redhat.com> - 4.1.1-29
- SAPInstance: add reload-action

  Resolves: rhbz#1751949

* Tue Jun 18 2019 Oyvind Albrigtsen <oalbrigt@redhat.com> - 4.1.1-28
- Initial build as separate package

  Resolves: rhbz#1688341

# vim:set ai ts=2 sw=2 sts=2 et:
